#!/bin/bash
iverilog -Wall -o cordic_tb.vvp ../rtl/cordic.v ./cordic_tb.v
vvp cordic_tb.vvp
gtkwave cordic_tb.vcd cordic_tb.gtkw