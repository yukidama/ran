/*********************************************************
*
* MIT license
* 
* Copyright 2023 Anhang Li
* 
* Permission is hereby granted, free of charge, to any
* person obtaining a copy of this software and associated
* documentation files (the "Software"), to deal in the
* Software without restriction, including without
* limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of
* the Software, and to permit persons to whom the Software
* is furnished to do so, subject to the following
* conditions:
* 
* The above copyright notice and this permission notice
* shall be included in all copies or substantial portions
* of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
* ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
* TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
* SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
* IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
* 
*********************************************************/
`timescale 1ns/1ps
module cordic_tb;

reg tb_clk_r;
reg tb_rst_nr;
reg [31:0] tb_z0_r;

reg     tb_start;

integer fd;

initial begin
	tb_start = 0;
    tb_clk_r = 0;
    tb_z0_r = 0;
    #1000
	fd = $fopen("log.txt","w");
    $dumpfile("cordic_tb.vcd");
    $dumpvars(0, cordic_tb);
    $readmemb("../constants/atan2.mem", DUT.atan_lut);
	tb_start = 1;
    #4000000
	tb_start = 0;
	$fclose(fd);
    $finish;
end

// Prescaling
// localparam tb_x0 = 32768/1.646760258;
localparam tb_x0 = 20000/1.646760258;
wire signed [16:0] xout;
wire signed [16:0] yout;

CORDIC DUT(
    .clk    (tb_clk_r),
    .z0     (tb_z0_r),
    .x0     (tb_x0),
    .y0     (16'd0),
    .xout_r (xout),
    .yout_r (yout)

);

always begin
    #5 tb_clk_r = ~tb_clk_r;
       tb_z0_r = tb_z0_r + 8192;
       // tb_z0_r = tb_z0_r + 4096;
end

always @(posedge tb_clk_r) begin
	if(tb_start) $fwrite(fd, "%x, %x, %x\n", tb_z0_r, xout, yout);
end






endmodule /* cordic_tb */

/* vim: set ts=4 sw=4 noet */
