module mvm #(
    parameter N = 64,
    parameter M = 32,
    parameter DWIDTH = 8
)(
    input  [DWIDTH-1:0]     m  [0:N-1][0:M-1],
    input  [DWIDTH-1:0]     v  [0:N-1],
    output [DWIDTH-1:0]     r  [0:N-1],
    output [(DWIDTH*2)-1:0] rx [0:N-1],
    output reg              overflow
);
    reg [(DWIDTH*3)-1:0] temp [0:N-1];
    always @* begin
        for (int i = 0; i < N; i = i + 1) begin
            temp[i] = 0;
        end
        for (int i = 0; i < N; i = i + 1) begin
            for (int j = 0; j < M; j = j + 1) begin
                temp[i] = temp[i] + m[i][j] * v[j];
            end
        end

        overflow = 0;
        for (int i = 0; i < N; i = i + 1) begin
            if (temp[i][(DWIDTH*3)-1:DWIDTH*2] > 0) begin
                overflow = 1;
            end
        end
    end

    genvar gi;
    generate
        for (gi = 0; gi < N; gi = gi + 1) begin: gen_loop1
            assign r[gi]  = temp[gi][((DWIDTH*2)-1):(DWIDTH)];
            assign rx[gi] = temp[gi][((DWIDTH*3)-1):(DWIDTH)];
        end
    endgenerate
endmodule
