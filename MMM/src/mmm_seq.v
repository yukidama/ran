
// Sequential MMM
//
//     |<----- K ----->|     |<-   N   ->|        |<--- N --->|
// --- +---+---+---+---+     +---+---+---+ ---    +---+---+---+ ---         
//  ^  |   |   |   |   |     |   |   |   |  ^     |   |   |   |  ^ 
//  M  +---+-- A --+---+  X  +---+---+---+  |  =  +---+ C +---+  M 
//  v  |   |   |   |   |     |   | B |   |  K     |   |   |   |  v 
// --- +---+---+---+---+     +---+---+---+  |     +---+---+---+ --- 
//                           |   |   |   |  v  
//                           +---+---+---+ --- 
//  
// Reusing the data path to refresh periodically
module mmm_seq#(
    parameter M = 16,      // Number of columns in A and rows in B
    parameter N = 16,      // Number of rows in A
    parameter K = 8,       // Number of columns in B
    parameter DWIDTH = 8,  // Number of bits in the data
    parameter FCLK = 100e6 // Clock frequency, need to be conservative
)(
    input               clk,
    input               rst,
    input               load_mat,
    input [DWIDTH-1:0]  Ain [N-1:0],    // Deposit one row of A
    input [DWIDTH-1:0]  Bin [N-1:0],    // Deposit one col of B
    output [DWIDTH-1:0] Cout [M-1:0],   
    // output reg          ack,            // Result available
    output              refresh_pending
);

// Compute parameters
// localparam COMPUTE_CYCLE       = K;
localparam LOAD_CYCLE          = M;
localparam REFRESH_DELAY       = (0.1e-3);
localparam REFRESH_DELAY_CYCLE = (REFRESH_DELAY * FCLK);
`define S_IDLE    0
`define S_LOAD    1
`define S_REFRESH 2
`define S_COMPUTE 3
`define S_MAX     `S_COMPUTE

// State machines
reg [$clog2(`S_MAX)-1:0] ss; 
reg [$clog2(LOAD_CYCLE):0]                         cnt_r;         // Compute / Load cycle counter
reg [$clog2($realtobits(REFRESH_DELAY_CYCLE))-1:0] cnt_refresh_r; // Refresh counter

// -- Fake RAM for A
wire [DWIDTH-1:0] Arow [N-1:0];
wire [DWIDTH-1:0] A    [M-1:0][N-1:0];
wire [DWIDTH-1:0] din [N-1:0];
assign din = (ss==`S_LOAD) ? Ain : Arow;
sp_sync_ram_2d #( 
    .DWIDTH(DWIDTH),.M(M),.N(N)
) u_ram_a (
    .clk (clk),
    .rst (rst),
    .we  (ss==`S_LOAD||ss==`S_REFRESH),
    .addr((ss==`S_LOAD||ss==`S_REFRESH)?cnt_r[$clog2(M)-1:0]:0),
    .din (din),
    .dout(Arow),
    .mem (A)
);

// -- MVM 
// Result (before shifting)
wire [(DWIDTH)-1:0]    r [0:M-1];
wire                   of;
wire [(DWIDTH*2)-1:0] rx [0:M-1];
reg [DWIDTH-1:0] Cout_msb [M-1:0];
always @(*) begin
    for (int i = 0; i < M; i = i + 1) begin
        Cout_msb[i] = rx[i][(DWIDTH*2)-1:DWIDTH];
    end
end
assign Cout = Cout_msb;
mvm #(.N(N),.M(M)) u_mvm(
    .m(A),
    .v(Bin),
    .r(r),
    .rx(rx),
    .overflow(of)
);

// State machine
assign refresh_pending = (ss==`S_REFRESH) ? 1 : 0;
reg refresh_flag_r;
wire comp_done = (cnt_r==(LOAD_CYCLE-1)) ? 1 : 0;

always @(posedge clk or posedge rst) begin
    if (rst) begin
        ss              <= `S_COMPUTE;
        cnt_r           <= 0;
        cnt_refresh_r   <= 0;
        refresh_flag_r  <= 0;
    end else begin
        if (1'b1) begin
            case (ss)
                `S_LOAD: begin      // Load Mat A from RAM
                    if (cnt_r >= (LOAD_CYCLE-1)) begin  // Loading Done
                        cnt_r <= 0;
                        ss <= `S_COMPUTE;
                    end else begin
                        cnt_r <= cnt_r + 1;

                    end
                end
                `S_REFRESH: begin   // Self-Refreshing of Mat A
                    if(cnt_r >= (LOAD_CYCLE-1)) begin
                        cnt_r <= 0;
                        ss <= `S_COMPUTE;
                    end else begin
                        cnt_r <= cnt_r + 1;
                    end
                    ss <= `S_COMPUTE;
                end
                `S_COMPUTE: begin   // Compute MVM of A and one col of B
                    if (load_mat) begin
                        ss <= `S_LOAD;
                        cnt_r <= 0;
                    end else begin
                        if (cnt_refresh_r >= (REFRESH_DELAY_CYCLE-1)) begin
                            cnt_refresh_r <= 0;
                            cnt_r <= 0;
                            refresh_flag_r <= 1;
                            if(comp_done) begin
                                ss <= `S_REFRESH;
                            end
                        end else begin
                            // Refresh Counter is still going
                            if(cnt_r >= (LOAD_CYCLE-1)) begin
                                cnt_r <= 0;
                                if(refresh_flag_r) begin
                                    ss <= `S_REFRESH;
                                    cnt_refresh_r <= 0;
                                end
                            end else begin
                                cnt_r <= cnt_r + 1;
                            end
                            cnt_refresh_r <= cnt_refresh_r + 1;
                        end
                    end
                end
            endcase
        end
    end
end

    
endmodule /* mmm_seq */
