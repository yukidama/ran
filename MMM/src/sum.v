`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:53:12 03/15/2016 
// Design Name: 
// Module Name:    SumElements 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: Gets the sum of the elements of a vector
//
//////////////////////////////////////////////////////////////////////////////////

//Description: sums the elements of a vector of size N, with DWIDTH bits for each position
module sum #(parameter N=2, parameter DWIDTH=16)(
	input [N*DWIDTH-1:0] a, //vector
	output [DWIDTH-1:0]b // result
    );
	 
/* verilator lint_off UNOPTFLAT */
	genvar i,j;
	parameter log=$clog2(N)+1; //Number of layers of the structure
	
	wire [DWIDTH-1:0] summat[0:2**log-1][0:log]; //Matrix to keep the partial values
	
	//Initializes the first layer of the structure
	generate
		for (i = 0; i < 2**log; i = i + 1) begin: gen_loop1
			if(i<N) begin : gen_if1
				assign summat[i][0] = a[N*DWIDTH-i*DWIDTH-1:N*DWIDTH-i*DWIDTH-DWIDTH];
			end else begin : gen_else1
				assign summat[i][0] = 0;
			end
		end
		for (i = 1; i <= log; i = i + 1) begin: gen_loop2
			for (j = 0; j <2**log/(2**i) ; j = j + 1) begin: gen_loop3
			assign summat[j][i] = summat[2*j][i-1] + summat[2*j+1][i-1];
			end
		end
	endgenerate
	
	// The result can be found in the last layer
	assign b=summat[0][log];

/* verilator lint_on UNOPTFLAT */
endmodule
