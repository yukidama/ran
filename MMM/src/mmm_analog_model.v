/* 
* MIT License
* 
* Copyright (c) 2024 Anhang Li (thelithcore@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
`timescale 1ns/1ps
module mmm_analog_model#(
    parameter NMK       = 16,                       // Only supporting M=N=K for now
    parameter SCALING   = (256.0/(NMK*256*256)),    // Scaling factor, Mapping the maximum value to 256
    parameter DWIDTH    = 8                         // Data width
)(
    input      [DWIDTH-1:0] a [0:NMK-1],    // A loader
    input      [DWIDTH-1:0] b [0:NMK-1],    // B loader
    output reg [DWIDTH-1:0] r [0:NMK-1],    // Result matrix output
    input                   wen,            // Write enable
    input                   clk,            // Clock
    input                   rst_n,          // Active low reset
    output                  dav             // Data available
);
// Matrix vector multiplication
localparam S_IDLE   = 2'b00;    // Idle state, This state is reserved for LPM
localparam S_LOAD_A = 2'b01;    // Loading the A matrix
localparam S_LOAD_B = 2'b10;    // Loading the B matrix and do the computation
localparam DELAY    = 2;      // How many cycles till the result is ready
reg [1:0]                   ss_r;
reg [$clog2(NMK+DELAY)-1:0] cnt_r;
always @(posedge clk or negedge rst_n) begin
    if (~rst_n) begin
        ss_r  <= S_IDLE;
        cnt_r <= 0;
    end else begin
        case (ss_r)
            S_IDLE: begin
                if (wen) begin
                    ss_r  <= S_LOAD_A;
                    cnt_r <= 1;
                end
            end
            // Loading A Matrix
            S_LOAD_A: begin
                if (cnt_r == NMK-1) begin
                    ss_r  <= S_LOAD_B;
                    cnt_r <= 0;
                end else begin
                    cnt_r <= cnt_r + 1;
                end
            end
            // Loading B Matrix
            S_LOAD_B: begin
                if (cnt_r == NMK+DELAY-1) begin
                    if(wen) ss_r <= S_LOAD_A;
                    else    ss_r <= S_IDLE;
                    cnt_r <= 0;
                end else begin
                    cnt_r <= cnt_r + 1;
                end
            end
            default: ss_r <= S_IDLE;
        endcase
    end
end

assign dav = ((ss_r == S_LOAD_B)&&(cnt_r>DELAY));

// modeling the memory of A
reg [DWIDTH-1:0] amem      [0:NMK-1][0:NMK-1];
real             amem_real [0:NMK-1][0:NMK-1];
real             b_real    [0:NMK-1];
real             r_real    [0:NMK-1];
reg [DWIDTH-1:0] r_d0  [0:NMK-1];
reg [DWIDTH-1:0] r_d1  [0:NMK-1];
reg [DWIDTH-1:0] r_d2  [0:NMK-1];
reg [DWIDTH-1:0] r_d3  [0:NMK-1];
always @(posedge clk or negedge rst_n) begin
    if (~rst_n) begin
        for (int i = 0; i < NMK; i = i + 1) begin
            for (int j = 0; j < NMK; j = j + 1) begin
                amem[i][j] <= {$random}[DWIDTH-1:0];
            end
        end
    end else begin
        if ((ss_r == S_LOAD_A)||(wen&&(ss_r == S_IDLE))) begin
            for (int i = 0; i < NMK; i = i + 1) begin
                    amem[i][cnt_r[$clog2(NMK)-1:0]] <= a[i];
            end
        end
    end

    if (ss_r == S_LOAD_B) begin
        for (int i = 0; i < NMK; i = i + 1) begin
            r_d3[i] <= r_d2[i];
            r_d2[i] <= r_d1[i];
            r_d1[i] <= r_d0[i];
        end
    end
end

always @* begin
    for (int i = 0; i < NMK; i = i + 1) begin
        for (int j = 0; j < NMK; j = j + 1) begin
            amem_real[i][j] = amem[i][j];
        end
        b_real[i] = b[i];
        r_d0[i]   = {$rtoi(r_real[i] * SCALING)}[DWIDTH-1:0];
        r[i]      = r_d3[i];
    end
end

// Assign the result


mvm_analog_model #(.N(NMK), .M(NMK), .TRANSPOSE(1)) u_mvm(.m(amem_real), .v(b_real), .r(r_real));

endmodule /* mmm_analog_model */