/* 
* MIT License
* 
* Copyright (c) 2024 Anhang Li (thelithcore@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
`timescale 1ns/1ps
// Pure Combinational
module mvm_analog_model #(
    parameter TRANSPOSE = 0,
    parameter N = 16,
    parameter M = 16
)(
    // m matrix input
    input  real m  [0:N-1][0:M-1],
    // v vector input
    input  real v  [0:N-1],
    // Result vector output
    output real r  [0:N-1]
);

// Matrix vector multiplication
real temp [0:N-1];
always @* begin
    for (int i = 0; i < N; i = i + 1) begin
        temp[i] = 0;
        for (int j = 0; j < M; j = j + 1) begin
            // Transpose M
            if (TRANSPOSE == 1) temp[i] = temp[i] + m[j][i] * v[j];
            else                temp[i] = temp[i] + m[i][j] * v[j];
        end
    end
end

genvar gi;
generate
    for (gi = 0; gi < N; gi = gi + 1) begin: gen_loop1
        assign r[gi]  = temp[gi];
    end
endgenerate

endmodule /* mvm_analog_model */
