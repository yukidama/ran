`timescale 1ns / 1ps
module innerproduct #(
    parameter DWIDTH = 8,
    parameter N      = 4
)(
  input wire  [(DWIDTH*N)-1:0] a,
  input wire  [(DWIDTH*N)-1:0] b,
  output wire [DWIDTH-1:0]     y
);
    wire [DWIDTH-1:0] Amat[0:N-1];
    wire [DWIDTH-1:0] Bmat[0:N-1];
    wire [(DWIDTH*N)-1:0] prods;
    wire [DWIDTH-1:0] prods_matrix[0:N-1];
    genvar i;
    generate
        for (i = 0; i < N; i = i + 1) begin: dp_loop
        assign Amat[i]         = a[(DWIDTH*N)-1-i*DWIDTH:N*DWIDTH-i*DWIDTH-DWIDTH];
        assign Bmat[i]         = b[(DWIDTH*N)-1-i*DWIDTH:N*DWIDTH-i*DWIDTH-DWIDTH];
        assign prods_matrix[i] = {Amat[i]*Bmat[i]}>>DWIDTH;
        assign prods[(DWIDTH*N)-1-i*DWIDTH:N*DWIDTH-i*DWIDTH-DWIDTH] = prods_matrix[i];
        end
    endgenerate
    sum #(.N(N),.DWIDTH(DWIDTH)) sums (
        .a(prods), 
        .b(y)
    );

endmodule /* innerproduct */
