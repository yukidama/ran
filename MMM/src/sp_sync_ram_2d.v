
module sp_sync_ram_2d #(
    parameter DWIDTH = 8, 
    parameter M = 16, 
    parameter N = 16
)(
    input wire                   clk, 
    input wire                   rst, 
    input wire                   we, 
    input wire [$clog2(M)-1:0]   addr, 
    input wire [DWIDTH-1:0]      din  [N-1:0],
    output reg [DWIDTH-1:0]      dout [N-1:0],
    output reg [DWIDTH-1:0]      mem  [M-1:0][N-1:0]
);
    wire [DWIDTH-1:0] dout_comb [N-1:0];
    integer i, j;
    always @(posedge clk or posedge rst) begin
        if (rst) begin
            for (i = 0; i < M; i = i + 1) begin
                for (j = 0; j < N; j = j + 1) begin
                mem[i][j] <= {DWIDTH{1'b0}};
                end
            end
            for (i = 0; i < N; i = i + 1) begin
                dout[i] <= {DWIDTH{1'b0}};
            end
        end else begin
            if (we) begin
                mem[addr] <= din;
            end
            
            dout <= dout_comb;
        end
    end
    assign dout_comb = mem[addr];
endmodule
