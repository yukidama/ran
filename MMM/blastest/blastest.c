#include <stdio.h>
#include <stdlib.h>
#include <cblas.h>
#include <time.h>
#include <stdint.h>
#include <math.h>
#include <memory.h>

#define I 4
#define J 5
#define K 6

void print_matrix(double* matrix, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%d ", (int)(round(matrix[i * cols + j])));
        }
        printf("\n");
    }
}

#define DTYPE uint8_t
#define DTYPE_MAX 255

double* mmm_naive(double* A, double* B, double* C1, int ii, int ji, int ki) {
    // Calculate C1 = A * B manually
    memset(C1, 0, ii * ki * sizeof(double));
    for (int k = 0; k < ki; k++) {
        for (int i = 0; i < ii; i++) {
            for (int j = 0; j < ji; j++) {
                C1[i * ki + k] += A[i * ji + j] * B[j * ki + k];
            }
        }
    }
    // Round C1 to integer
    for (int i = 0; i < ii * ki; i++) {
        C1[i] = round(C1[i]);
    }
    return C1;
}

double* mvm_reference(double* A, double* x, double* y, int ii, int ji) {
    // Calculate y = A * x using matrix-vector multiplication
    // This function will be implemented as an analog module
    cblas_dgemv(CblasRowMajor, CblasNoTrans, ii, ji, 1.0, A, ji, x, 1, 0.0, y, 1);
    return y;
}

double* mvm_naive(double* A, double* x, double* y, int ii, int ji) {
    // Calculate y = A * x using matrix-vector multiplication
    memset(y, 0, ii * sizeof(double));
    for (int i = 0; i < ii; i++) {
        for (int j = 0; j < ji; j++) {
            y[i] += A[i * ji + j] * x[j];
        }
    }
    return y;
}

double* mmm_reference(double* A, double* B, double* C1, int ii, int ji, int ki) {
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, ii, ki, ji, 1.0, A, ji, B, ki, 0.0, C1, ki);
    
    return C1;
}

double* mmm_optimized(double* A, double* B, double* C1, int ii, int ji, int ki) {
    // Calculate C = A * B using matrix-vector multiplication
    memset(C1, 0, ii * ki * sizeof(double));
    // Implement mmm using mvm_reference
    double y[ki];
    double x[ki];
    for (int k = 0; k < ki; k++) {
        // Obtain the k-th column of B (Broadcasting)
        for (int j = 0; j < ki; j++) {
            x[j] = B[j * ki + k];
        }
        // Calculate the k-th column of C
        mvm_reference(A, x, y, ii, ji);
        // Store the result in C
        for (int i = 0; i < ii; i++) {
            C1[i * ki + k] = y[i];
        }
    }
    // Round C1 to integer
    for (int i = 0; i < ii * ki; i++) {
        C1[i] = round(C1[i]);
    }
    return C1;
}

double mm_rmse(double* A, double* B, int ii, int ji) {
    double rmse = 0;
    for (int i = 0; i < ii; i++) {
        for (int j = 0; j < ji; j++) {
            rmse += pow(A[i * ji + j] - B[i * ji + j], 2);
        }
    }
    return sqrt(rmse / (ii * ji));
}

int main(int argc, char** argv) {
    // if no seed provided in argv, use time as the seed
    int seed = 0;
    if (argc < 2) {
        seed = time(NULL);
    } else {
        seed = atoi(argv[1]);
    }
    srand(seed);
    printf("Seed: %d\n", seed);

    
    double* A  = (double*)malloc(I * J * sizeof(double));
    double* B  = (double*)malloc(J * K * sizeof(double));
    double* C  = (double*)malloc(I * K * sizeof(double));
    double* C1 = (double*)malloc(I * K * sizeof(double));

    // Randomly generate A
    for (int i = 0; i < I; i++) {
        for (int j = 0; j < J; j++) {
            A[i * J + j] = ((double)rand() / RAND_MAX)*(DTYPE_MAX+1)-1;
        }
    }

    // Randomly generate B
    for (int i = 0; i < J; i++) {
        for (int j = 0; j < K; j++) {
            B[i * K + j] = ((double)rand() / RAND_MAX)*(DTYPE_MAX+1)-1;
        }
    }

    mmm_reference(A, B, C, I, J, K);
    // mmm_manual(A, B, C1, I, J, K);
    mmm_optimized(A, B, C1, I, J, K);
    // Divide-by-256 and round all elements of C
    for (int i = 0; i < I * K; i++) {
        C[i] = round(C[i] / 256.0 + 0.5);
        C1[i] = round(C1[i] / 256.0 + 0.5);
    }
    
    printf("Matrix A:\n");
    print_matrix(A, I, J);

    printf("Matrix B:\n");
    print_matrix(B, J, K);

    printf("Matrix C = A * B:\n");
    print_matrix(C, I, K);

    printf("Matrix C1 = A * B:\n");
    print_matrix(C1, I, K);

    printf("RMSE: %f\n", mm_rmse(C, C1, I, J));

    free(A);
    free(B);
    free(C);
    free(C1);

    return 0;
}
