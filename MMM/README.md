# Matrix-Matrix Multipler (M3)

This folder contains a verilog implementation of a fixed-point 8-bit matrix-matrix multipler implemented using a matrix-vector multiplier. A golden block built using BLAS is provided as a reference

## Timing

The Amat memory is implemented as a dynamic memory. Thus it need periodic refresh to retain the data. From simulation in 180nm, the leakage of the MOMCAP is quite high, such that the refresh rate should be at least 10kHz to retain an equivalent of 8-bit (256 levels) of analog info per cell. To avoid occupying too much area and potentially improve energy efficiency. The circuit uses a single 10-bit 40MSps ADC-DAC pair for refreshing and data readout. 

$$M=K=16$$
$$N=8$$
$$ENOB\ge8.0bits$$
$$f_{sys} = 40 MHz$$
$$f_{refresh} = 10 kHz$$

Cycle time for full A-matrix operations:
$$\tau_{refresh,A} = (M\times K)/f_{sys} = 6.4\mu s$$
$$\tau_{compute} = (K\times N)/f_{sys} = 3.2\mu s$$

The number of matrix multiplication can be done in one refresh cycle is:
$$N_{compute}:N_{refresh,A}=(1/f_{refresh}-\tau_{refresh,A})/\tau_{compute}=29.25:1$$

## Area Estimation



## Power Consumption