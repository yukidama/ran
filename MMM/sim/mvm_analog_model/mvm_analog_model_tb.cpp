#include <verilated.h>
#include "Vmvm_analog_model.h"
#include <cstdlib>
#include <cblas.h>

#define N 16
#define M 16
#define LOOP 100
// Reference implementation of the inner product using BLAS
double* mvm_ref(double m[N][M], double v[N], double r[N], double rx[N]) {
    double* A = (double*)malloc(N * M * sizeof(double));
    double* x = (double*)malloc(M * sizeof(double));
    double* y = (double*)malloc(N * sizeof(double));
    // Load the input
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
            A[i * M + j] = m[i][j];
    for (int i = 0; i < M; i++)
        x[i] = v[i];
    // Perform the matrix-vector multiplication
    cblas_dgemv(CblasRowMajor, CblasNoTrans, N, M, 1.0, A, M, x, 1, 0.0, y, 1);
    // Convert the output to uint8_t
    for (int i = 0; i < N; i++) {
        r[i]  = (uint8_t)floor((y[i]/256.0));
        rx[i] = (uint16_t)floor(y[i]/256.0);
    }
    free(A);
    free(x);
    free(y);
    return r;
}

int main(int argc, char** argv) {
    Verilated::commandArgs(argc, argv);
    // Seed the RNG using time
    srand(time(NULL));
    // Create an instance of the innerproduct module
    Vmvm_analog_model* mvm = new Vmvm_analog_model;
    double worse_rmse = 0;
    int    worse_rmse_run = 0;
    int    maxval = 0;

    mvm->eval();

    // Show the type of innerproduct->a
    // Toggle the clock and evaluate the module
    for (int i = 0; i < LOOP; i++) {
        // Randomize the inputs
        for (int x = 0; x < N; x++)
            for (int y = 0; y < M; y++)
                mvm->m[x][y] = (double)(rand() % 256);
        for (int x = 0; x < N; x++)
            mvm->v[x] = (double)(rand() % 256);

        mvm->eval();

        // Compare the output to the reference
        double r[N];
        double rx[N];
        double mvm_m_f64[N][M];
        double mvm_v_f64[N];
        for (int x = 0; x < N; x++) {
            for (int y = 0; y < M; y++){
                mvm_m_f64[x][y] = mvm->m[x][y];
            }
            mvm_v_f64[x] = mvm->v[x];
        }
        mvm_ref(mvm_m_f64, mvm_v_f64, r, rx);

        // Print the inputs and outputs
        printf("M  :[");
        for (int x = 0; x < N; x++) {
            for (int y = 0; y < M; y++){
                printf("%.2f", mvm->m[x][y]);
                if (y < M-1)
                    printf(" ");
            }
            if(x < N-1)
                printf(", ");
        }
        printf("]\n");
        printf("V  :[");
        for (int x = 0; x < N; x++)
            if (x == N - 1)
                printf("%.2f", mvm->v[x]);
            else
                printf("%.2f, ", mvm->v[x]);
        printf("]\n");
        printf("Out:");
        for (int x = 0; x < N; x++)
            printf("%.2f ", mvm->r[x]/256.0);
        printf("\n");
        printf("Ref:");
        for (int x = 0; x < N; x++){
            printf("%.2f ", rx[x]);
            if (rx[x] > maxval)
                maxval = rx[x];
        }
        printf("\n");

        // Calculate RMSE
        double rmse = 0;
        for (int x = 0; x < N; x++) {
            rmse += (mvm->r[x]/256.0 - rx[x]) * (mvm->r[x]/256.0 - rx[x]);
        }
        rmse = sqrt(rmse / N);
        if (rmse > worse_rmse) {
            worse_rmse = rmse;
            worse_rmse_run = i;
        }
        // Print RMSE in bold
        printf("\033[1mRUN: %d, RMSE: %.2f\033[0m\n", i, rmse);
    }
    // Print in bold green
    printf("\033[1;32mWORSE RMSE: %.2f @ RUN: %d\033[0m\n", worse_rmse, worse_rmse_run);
    printf("\033[1;32mMAXVAL: %d\033[0m\n", maxval);
    
    // Clean up
    mvm->final();
    delete mvm;
    
    return 0;
}
