#include <verilated.h>
#include "Vmvm.h"
#include <cstdlib>
#include <cblas.h>

#define N 64
#define M 32
#define LOOP 100
// Reference implementation of the inner product using BLAS
uint8_t* mvm_ref(uint8_t m[N][M], uint8_t v[N], uint8_t r[N], uint16_t rx[N]) {
    double* A = (double*)malloc(N * M * sizeof(double));
    double* x = (double*)malloc(M * sizeof(double));
    double* y = (double*)malloc(N * sizeof(double));
    // Load the input
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
            A[i * M + j] = m[i][j];
    for (int i = 0; i < M; i++)
        x[i] = v[i];
    // Perform the matrix-vector multiplication
    cblas_dgemv(CblasRowMajor, CblasNoTrans, N, M, 1.0, A, M, x, 1, 0.0, y, 1);
    // Convert the output to uint8_t
    for (int i = 0; i < N; i++) {
        r[i] = (uint8_t)floor((y[i]/256.0));
        rx[i] = (uint16_t)floor(y[i]/256.0);
    }
    free(A);
    free(x);
    free(y);
    return r;
}

int main(int argc, char** argv) {
    Verilated::commandArgs(argc, argv);
    // Seed the RNG using time
    srand(time(NULL));
    // Create an instance of the innerproduct module
    Vmvm* mvm = new Vmvm;
    double worse_rmse = 0;
    int    worse_rmse_run = 0;
    int    maxval = 0;

    mvm->eval();

    // Show the type of innerproduct->a
    // Toggle the clock and evaluate the module
    for (int i = 0; i < LOOP; i++) {
        // Randomize the inputs
        for (int x = 0; x < N; x++)
            for (int y = 0; y < M; y++)
                mvm->m[x][y] = rand() % 256;
        for (int x = 0; x < N; x++)
            mvm->v[x] = rand() % 256;

        mvm->eval();

        // Compare the output to the reference
        uint8_t r[N];
        uint16_t rx[N];
        mvm_ref(mvm->m, mvm->v, r, rx);

        // Print the inputs and outputs
        printf("M  :[");
        for (int x = 0; x < N; x++) {
            for (int y = 0; y < M; y++){
                printf("%d", mvm->m[x][y]);
                if (y < M-1)
                    printf(" ");
            }
            if(x < N-1)
                printf(", ");
        }
        printf("]\n");
        printf("V  :[");
        for (int x = 0; x < N; x++)
            if (x == N - 1)
                printf("%d", mvm->v[x]);
            else
                printf("%d, ", mvm->v[x]);
        printf("]\n");
        printf("Out:");
        for (int x = 0; x < N; x++)
            printf("%d ", mvm->rx[x]);
        printf("\n");
        printf("Ref:");
        for (int x = 0; x < N; x++){
            printf("%d ", rx[x]);
            if (rx[x] > maxval)
                maxval = rx[x];
        }
        printf("\n");
        // check overflow
        if(mvm->overflow){
            // Print in bold red
            printf("\033[1;31mOverflow\033[0m\n");
        }

        // Calculate RMSE
        double rmse = 0;
        for (int x = 0; x < N; x++) {
            rmse += (mvm->rx[x] - rx[x]) * (mvm->rx[x] - rx[x]);
        }
        rmse = sqrt(rmse / N);
        if (rmse > worse_rmse) {
            worse_rmse = rmse;
            worse_rmse_run = i;
        }

        // Print RMSE in bold
        printf("\033[1mRUN: %d, RMSE: %f\033[0m\n", i, rmse);
    }
    // Print in bold green
    printf("\033[1;32mWORSE RMSE: %f @ RUN: %d\033[0m\n", worse_rmse, worse_rmse_run);
    printf("\033[1;32mMAXVAL: %d\033[0m\n", maxval);
    
    // Clean up
    mvm->final();
    delete mvm;
    
    return 0;
}
