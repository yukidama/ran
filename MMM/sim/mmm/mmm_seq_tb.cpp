#include <verilated.h>
#include <verilated_vcd_c.h>
#include "Vmmm_seq.h"
#include "Vmmm_seq___024root.h"
#include <cstdlib>
#include <cblas.h>
#include <iostream>

#define N 16
#define M 16
#define K 8
#define LOOP 100

#define CONCAT(a,b) a##b
#define SIGNAL(x) CONCAT(mmm->rootp->mmm_seq__DOT__,x)

void tick(Vmmm_seq* mmm, VerilatedVcdC* tfp, int i) {
    mmm->clk = 0;
    mmm->eval();
    if(tfp){
        tfp->dump(2 * i); // Dump the trace at the rising edge of the clock
    }
    mmm->clk = 1;
    mmm->eval();
    if(tfp){
        tfp->dump(2 * i + 1); // Dump the trace at the falling edge of the clock
        tfp->flush();
    }
}

int i = 0;

Vmmm_seq* mmm;
VerilatedVcdC* tfp;

void print_matrix(double* matrix, int rows, int cols) {
    for (int y = 0; y < rows; y++) {
        for (int x = 0; x < cols; x++) {
            std::cout<< (int)(round(matrix[y * cols + x])) << "\t";
        }
        std::cout << std::endl;
    }
}

int main(int argc, char** argv) {
        // Parameter parser
    int randomSeed = 0;
    int seed = 0xDEADBEEF;
    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "--random-seed") == 0) {
            randomSeed = 1;
        } else if (strcmp(argv[i], "--seed") == 0 && i + 1 < argc) {
            seed = atoi(argv[i + 1]);
            i++;
        }
    }
    // Seed the RNG using time or user-defined seed
    if (randomSeed) {
        srand(time(NULL));
    } else {
        srand(seed);
    }
    // Print seed in hex
    std::cout << "Seed: 0x" << std::hex << seed << std::dec << std::endl;

    Verilated::commandArgs(argc, argv);
    mmm = new Vmmm_seq;
    // Create an instance of the innerproduct module
    Verilated::traceEverOn(true);
    // Create a VCD trace file
    tfp = new VerilatedVcdC;
    mmm->trace(tfp, 99); // Set the trace depth to 99
    tfp->open("mmm_trace.vcd");

    // Preparing the simulation
    double A[M*N], B[N*K], C[M*K];

    // Randomize A
    for (int x = 0; x < M; x++) {     // M cols
        for (int y = 0; y < N; y++) { // N rows
            A[x * N + y] = (rand() % 256);
        }
    }
    // Randomize B
    for (int x = 0; x < N; x++) {       // N columns
        for (int y = 0; y < K; y++) {   // K rows
            B[x * K + y] = (rand() % 256);
        }
    }
    // Compute C using BLAS
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, \
                M, N, K, 1.0, A, N, B, K, 0.0, C, K);
    // Print matrix A
    std::cout << "Matrix A:" << std::endl;
    print_matrix(A, M, N);  
    // Print matrix B
    std::cout << "Matrix B:" << std::endl;
    print_matrix(B, N, K);
    
    // Print matrix C
    std::cout << "Expected Matrix C (BLAS):" << std::endl;
    for (int x = 0; x < M; x++) {   // M Columns
        for (int y = 0; y < K; y++) // K rows
            std::cout << C[x * N + y]/256.0/256.0 << "\t";
        std::cout << std::endl;
    }




    // Do the reset
    mmm->rst = 1;
    tick(mmm, tfp, i++);
    mmm->rst = 0;
    tick(mmm, tfp, i++);
    // -- Start the simulation
    

    // Loading Mat A
    // std::cout << "Matrix A:" << std::endl;
    mmm->load_mat = 1;
    tick(mmm, tfp, i++);
    for (int y = 0; y < M; y++) {
        for (int x = 0; x < N; x++){
            mmm->Ain[x] = int(A[y*N+x]);
        }
        if(y==1) mmm->load_mat = 0; 
        tick(mmm, tfp, i++);
    }
    // Mat A is loaded, check 
    for (int y = 0; y < M; y++) {
        for (int x = 0; x < N; x++){
            std::cout << int(SIGNAL(A)[x][y]) << "\t";
        }
        std::cout << std::endl;
    }

    // Send B row by row and acquire C
    mmm->load_mat = 0;
    for (int y = 0; y < K; y++) {
        for (int x = 0; x < N; x++){
            mmm->Bin[x] = B[y*N+x];
            // std::cout << int(mmm->Bin[x]) << "\t";
        }
        std::cout << std::endl;
        tick(mmm, tfp, i++);
        // Get the result
        for (int x = 0; x < M; x++){
            C[y*N+x] = SIGNAL(Cout)[x];
            std::cout << int(SIGNAL(Cout)[x]) << "\t";
        }
    }
    std::cout << std::endl;
    tick(mmm, tfp, i++);


    // Dump the trace for each clock cycle
    for (; i < LOOP; i++) {
        tick(mmm, tfp, i);
    }

    // Close the VCD trace file
    tfp->close();
    delete tfp;

    // Cleanup
    mmm->final();
    delete mmm;
    std::cout << "Simulation complete" << std::endl;

    return 0;
}