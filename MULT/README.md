# A Collection of Parameterized Multipliers

### booth_4.v: 
- Pipelined Radix-4 Booth Multiplier with adder height compression.
- Synthesizable to 1.2GHz @ 180nm.
- [Ref: Bewick, Gary W. Fast Multiplication: Algorithms and Implementations, Stanford](http://i.stanford.edu/pub/cstr/reports/csl/tr/94/617/CSL-TR-94-617.pdf)