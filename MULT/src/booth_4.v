// SPDX-FileCopyrightText: 2024 Anhang Li <thelithcore@gmail.com>
// SPDX-License-Identifier: MIT
/*********************************************************
*
* MIT license
* 
* Copyright 2024 Anhang Li
* 
* Permission is hereby granted, free of charge, to any
* person obtaining a copy of this software and associated
* documentation files (the "Software"), to deal in the
* Software without restriction, including without
* limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of
* the Software, and to permit persons to whom the Software
* is furnished to do so, subject to the following
* conditions:
* 
* The above copyright notice and this permission notice
* shall be included in all copies or substantial portions
* of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
* ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
* TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
* SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
* IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
* 
*********************************************************/

`timescale 1ns/1ps

// Radix-4 Booth Partial Product Generator
module booth_ppgen_r4 #(
    parameter DWIDTH = 8
)(
    input [DWIDTH-1:0]			a,
    input [2:0]					br4,
    output reg [DWIDTH:0]		o,
    output                      s
);
    assign s = br4[2];
    always @* begin
        o = {(DWIDTH+1){1'bx}};
        case(br4)
            3'b000: o = 0;
            3'b010: o = {1'b0,a};
            3'b001:	o = {1'b0,a};
            3'b011: o = {a,1'b0};
            3'b100: o = {~a,1'b1};
            3'b101:	o = {1'b1,~a};
            3'b110:	o = {1'b1,~a};
            3'b111: o = {DWIDTH+1{1'b1}};
        endcase
    end
endmodule /* booth_ppgen_r4 */

module mult(
    input [DWIDTH-1:0]  a,
    input [DWIDTH-1:0]  b,
    input               clk, 
    input               reset,
    output [OWIDTH-1:0] result
);
    localparam DWIDTH = 8;
    localparam OWIDTH = DWIDTH*2;
    localparam N_STAGES = 4;

    genvar gi;
    // Step 1. Booth Encoding
    localparam N_ENC  = DWIDTH/2+1;
    localparam PPSIZE = DWIDTH+1;
    wire [PPSIZE+1:0]         b_br4_enc_input = {2'b0, b,1'b0};
    wire [(PPSIZE*N_ENC)-1:0] b_br4_pp;
    wire [N_ENC-1:0]          b_br4_signs;
    generate
        for(gi=0;gi<N_ENC;gi=gi+1) begin: booth_pp_gen
            booth_ppgen_r4 #(.DWIDTH(DWIDTH)) u_ppgen(
                .a(a),
                .br4(b_br4_enc_input[(gi+1)*2:gi*2]),
                .o(b_br4_pp[(gi+1)*PPSIZE-1:gi*PPSIZE]),
                .s(b_br4_signs[gi])
            );
        end
    endgenerate
    // Step 2. CSA (Should be a Wallace Tree)
    reg [DWIDTH*2-1:0] s0_result;
    integer i;
    always @(*) begin
        s0_result = 0;
        for(i=0;i<N_ENC;i=i+1) begin: booth_csa_gen
            case(i)
                0:       s0_result = s0_result + ({{(DWIDTH-4){1'b0}},!b_br4_signs[0],{2{b_br4_signs[0]}},b_br4_pp[i*PPSIZE +: PPSIZE]});
                N_ENC-2: s0_result = s0_result + ({{(DWIDTH-4){1'b0}},!b_br4_signs[i],b_br4_pp[i*PPSIZE +: PPSIZE], 1'b0, b_br4_signs[i-1]} << ((i-1)*2));
                N_ENC-1: s0_result = s0_result + ({{(DWIDTH-3){1'b0}},b_br4_pp[i*PPSIZE +: PPSIZE], 1'b0, b_br4_signs[i-1]} << ((i-1)*2));
                default: s0_result = s0_result + ({{(DWIDTH-5){1'b0}},1'b1,!b_br4_signs[i],b_br4_pp[i*PPSIZE +: PPSIZE],1'b0, b_br4_signs[i-1]} << ((i-1)*2));
            endcase
        end

    end
    // Step 3. Auto Pipelining
    generate
        if(N_STAGES==0) assign result = s0_result;
        else begin : pipeline
            reg [OWIDTH*N_STAGES:0] pipeline_r;
            always @(posedge clk) begin
                if(reset) pipeline_r <= 0;
                else      pipeline_r <= {pipeline_r[OWIDTH*(N_STAGES-1):0], s0_result};
            end
            assign result = pipeline_r[OWIDTH*N_STAGES-1 : OWIDTH*(N_STAGES-1)];
        end
    endgenerate
endmodule /* mult */

