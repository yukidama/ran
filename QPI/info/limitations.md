# Known Limitations of the QPI Implementation in Calculators

Due to the limitation of this approximation algorithm. There are many situations where not-uncommon computations can 
generate real number results that QPI fails to rationalize. 
Those test cases will reveal the real boundary of their specific QPI implementations, and give away many of the
arbitrary constants they used in their code.

I've tested the following calculators on TI-30X pro, Casio fx-JP900CW and Casio fx-991es. It seems that all of them 
share exactly the same code at the bottom-level.

!! To avoid getting into trouble, I won't use the name of any particular manufacturer. Instead, I'll refer to those 
calculators as "DMSCs" (Dot-Matrix Scientific Calculators).

----
## List of limitations & interesting findings:

- p/q is limited to q < 100,000,000.
- 100001/100000 works, but 100000/100001 fails. Most likely a precision limitation.
- SQRT(p) is limited to p < 1000. The largest prime number you can get is SQRT(997)
- SQRT(p)/q is limited to q < 100. Rationalization will always fail when q >= 100 no matter what is the numerator.
- PI*p/q is limited to q < 100. Rationalization will always fail when q >= 100 no matter what is the numerator.
- 99*SQRT(997)+99*SQRT(995) works!
- 3.14159265358979323 = PI
- No matter how many digits are entered, the calculator does not recognize square-root from real numbers
- cos(30), cos(105) works as intended, but cos(36) = (1+sqrt(5))/4 does not, for no reason. This is either a precision issue or because 36 degrees is not hard-coded in the ROM.
----
## Analysis:

TBD...

That q<100 limitation is particularly suspicious! Because it's also the detection limit for any functions in the 
original QPI algorithm on HP48.

In the HP48 QPI, the SQRT detection is only limited by the dynamic range of the floating point numbers (p<50001). 
But it's different on those machines.

I have no idea about how the double sqrt detection is implemented. Brute-force search is not an option here.

