# QPI
The QPI algorithm is a decimal approximation algorithm, originally from the
`QPirac` (Quick PI/Rational) of MuPAD. This algorithm approximates a
decimal number using elementary functions or a combination them.

Because it's an approximation, the designer has to play with the numbers and
choose what kind of approximation he wants to try until the algorithm gives
desirable results.

Below is a comparison of different elementary functions known implementations
have included in the approximation routine:

``` text
+-----------+-----------+-----------------------+------------------------------+
| QPI43     | QPI42     | DMSC                  | Description                  |
| HP Saturn | HP-42s    | nx-U8/100             |                              |
| Assembly  | Keystroke | Assembly              |                              |
|-----------|-----------|-----------------------|------------------------------|
| P/Q       | P/Q       | P/Q                   | Rational Number              |
| π × P/Q   | π × P/Q   | π × P/Q               | Multiples of Pi              |
| √(P/Q)    | √(P/Q)    | -                     | Square Root                  |
| -         | -         | K*√(P)/Q              | Some enhanced SQRT det.      |
| -         | -         | (K1*√(P1)+P2)/Q       | Note: in DMSCs' case, P is a |
| -         | -         | (K1*√(P1)+K2*√(P2))/Q | prime up to 997              |
| LN(P/Q)   | LN(P/Q)   | -                     | Natural Logarithm            |
| E^(P/Q)   | E^(P/Q)   | -                     | Exponential                  |
| -         | -         | 0.3333...             | Detection of repeating digit |
|           |           |                       | (Available on some models)   |
+-----------+-----------+-----------------------+------------------------------+
```

The square-root detection on DMSC is unusual. I suspect that it's not implemented
using QPI. But I need more evidence to confirm that. 
I'm surprised that (99*√(997)+99*√(995))/90 works on DMSC.

## References
- [https://www.hpcalc.org/details/1403](https://www.hpcalc.org/details/1403)
- [https://www.hpmuseum.org/software/qpi/42sqpi.htm](https://www.hpcalc.org/details/1403)
- [https://www.hpmuseum.org/forum/thread-18.html](https://www.hpcalc.org/details/1403)
- [https://www.hpmuseum.org/cgi-sys/cgiwrap/hpmuseum/archv021.cgi?read=254757](https://www.hpcalc.org/details/1403)