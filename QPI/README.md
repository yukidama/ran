# Result Rationalization in DMSCs

!! To avoid getting into trouble, I won't use the name of any particular manufacturer. Instead, I'll refer to those
calculators as "DMSCs" (Dot-Matrix Scientific Calculators). Some DMSC examples are:

* CASIO fx-300ES
* CASIO fx-JP900N
* CASIO fx-991CW
* Sharp EL-W516T
* Canon F-789SGA
* TI-36X pro
* HP 300s+
* and numerous clones

Those models are marked by their capability to show the result in a rational form, which gives the illusion of a CAS system. 
I believe all of those models, no matter new or old, share exactly the same underlying code to perform those computations.

The goal of this project is to recreate the algorithm in C.

Note: This is NOT a real CAS system. It's not even close. What kind of
approximation can be done is completely hard-coded. For example,
(√(2)+√(3)+√(5))/2 does not work on DMSC because its algorithm only tests
two square-rooted nominators. It's not an accuracy limitation, nor a
RAM / ROM / computation time limit.

## Getting Started
This repo is an implementation of the decimal QPI algorithm using the intel
BCD library. **Only tested on X86_64 Ubuntu-22.04.**
Should be portable if you have a good BCD library that does basic math.

Note: The rationalization is done in the decimal-base, so you have to use BCD math to get the correct result.

The following command will compile the intel BCD library and the QPI program, then it will run the QPI program.

```
make QPI
```