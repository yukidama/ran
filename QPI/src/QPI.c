/******************************************************************************
*
*    Copyright (C) 2023  Anhang Li
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published
*    by the Free Software Foundation, either version 3 of the License, or
*    any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
******************************************************************************/

// -- Settings
#define LINUX 
// - Enable printing of debug messages
// #define QPI_DEBUG
// - Use my end-point limit for factorization
// #define QPI_LIM_BY_NITER

// -- SQRT Related Settings
// - The largest sqrt(p) CASIO can do is exactly 1000
// - any p beyond that will cause a give-up.
// #define QPI_SQRT_PLIMIT 1000

// - How many digits of accuracy do you want?
#define QPI_ACC_LIMIT  9

#define PRINT_BID64(NAME,VAR)  do{bid64_to_string(buf,&(VAR));printf(GRN ""NAME" = " UGRN "%s\n" COLOR_RESET, buf);}while(0);
#define PRINT_BID128(NAME,VAR) do{bid128_to_string(buf,&(VAR));printf(GRN ""NAME" = " UGRN "%s\n" COLOR_RESET, buf);}while(0);

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "bid_conf.h"
#include "bid_functions.h"
#include "ANSI-color-codes.h"

char buf[128];
char dline[128];
int pii, qii;

typedef struct{
    BID_UINT64 p;
    BID_UINT64 q;
} BID_PQ_t;

typedef enum {
    QPI_TYPE_FRAC,
    QPI_TYPE_SQRT,      // The Original QPI SQRT Implementation ±SQRT(p/q)
    QPI_TYPE_FRAC_S,    // Special: Simplified FRAC, ±k1*SQRT(p)/q
    QPI_TYPE_SQRT_S,    // Special: Simplified SQRT, ±k1*SQRT(p)/q
    QPI_TYPE_PI,
    QPI_TYPE_LN,
    QPI_TYPE_EXP,
    QPI_TYPE_INVALID
}QPI_DTYPE_T;

typedef struct {
    BID_PQ_t pq;
    BID_PQ_t pq2; // p2 is k1, q2 is unused
    QPI_DTYPE_T dtype;
}QPI_D_T;

QPI_DTYPE_T QPI_print(QPI_D_T     d);
QPI_D_T     QPI_auto (BID_UINT64* a);
QPI_D_T     QPI_sqrt (BID_UINT64* a);
QPI_D_T     QPI_pi   (BID_UINT64* a);
QPI_D_T     QPI_ln   (BID_UINT64* a); // Not Implemented
QPI_D_T     QPI_exp  (BID_UINT64* a); // Not Implemented
QPI_D_T     QPI_frac (BID_UINT64* a);

QPI_DTYPE_T QPI_print(QPI_D_T d){
    switch(d.dtype){
        case QPI_TYPE_FRAC:
            bid64_to_int32_floor(&pii,&(d.pq.p));
            bid64_to_int32_floor(&qii,&(d.pq.q));
            if(qii==1) sprintf(dline, "%d", pii);
            else       sprintf(dline, "%d/%d", pii, qii);
#ifdef QPI_DEBUG
            printf("Rational   : ");
#endif /* QPI_DEBUG */
            printf("%s\n", dline);
            break;
        case QPI_TYPE_SQRT:
            bid64_to_int32_floor(&pii,&(d.pq.p));
            bid64_to_int32_floor(&qii,&(d.pq.q));
            // if(qii==1) sprintf(dline, "√%d", pii);
            // else       sprintf(dline, "√%d/%d", pii, qii);
            // int len = strlen(dline);
            // printf("              ");
            // for(int i=0;i<len-3;i++) printf("_");
            // printf("\n");
            if(qii==1) sprintf(dline, "√(%d)", pii);
            else       sprintf(dline, "√(%d/%d)", pii, qii);
#ifdef QPI_DEBUG
            printf("Square Root: ");
#endif /* QPI_DEBUG */
            printf("%s\n", dline);
            break;
        case QPI_TYPE_PI:
            bid64_to_int32_floor(&pii,&(d.pq.p));
            bid64_to_int32_floor(&qii,&(d.pq.q));
            if(qii==1) sprintf(dline, "π*%d", pii);
            else       sprintf(dline, "π*%d/%d", pii, qii);
#ifdef QPI_DEBUG
            printf("Pi         : ");
#endif /* QPI_DEBUG */
            printf("%s\n", dline);
            break;
        default:
            printf("Invalid Type : %d", d.dtype);
            break;
    }
    return d.dtype;
}

QPI_D_T QPI_auto(BID_UINT64* a){
    QPI_D_T rtn;
    int cond;
    int is_negative;
    BID_UINT64 t;
    BID_UINT64 zero;
    bid64_from_string(&zero, "0");
    bid64_isNormal(&cond, a);
    if(!cond) {
        rtn.dtype=QPI_TYPE_INVALID;
        return rtn;
    }
    bid64_quiet_less(&is_negative, a, &zero);
    if(is_negative) bid64_negate(&t, a);
    else            memcpy(&t, a, sizeof(BID_UINT64));
    QPI_D_T dlist[3];
    dlist[0] = QPI_frac(&t);
    dlist[1] = QPI_sqrt(&t);
    dlist[2] = QPI_pi  (&t);
    BID_UINT64  minq;
    int         minqi = 0;
    memcpy(&minq, &(dlist[0].pq.q), sizeof(BID_UINT64));

    for(int i=0;i<sizeof(dlist)/sizeof(QPI_D_T);i++){
        // Find the item with smallest q
        if(dlist[i].dtype==QPI_TYPE_INVALID)       continue;
        bid64_quiet_less(&cond, &(dlist[i].pq.q), &minq);
        if(cond) {
            minqi = i;
            memcpy(&minq, &(dlist[i].pq.q), sizeof(BID_UINT64));
        }
    }
    rtn = dlist[minqi];
    if(is_negative) bid64_negate(&(rtn.pq.p), &(rtn.pq.p)); // Note: sqrt(-p/q) is actually -sqrt(p/q)
#ifdef QPI_DEBUG
    printf(GRN "Selecting %d\n" COLOR_RESET, minqi);
#endif /* QPI_DEBUG */
    return rtn;
}

// QPI_sqrt
//
// This function factors a BID64 number into SQRT(P/Q) form to a given accuracy.
// Input:  a:    The BID64 number to be factorized, must be positive
// Output: d:    QPI Object
QPI_D_T QPI_sqrt(BID_UINT64* a){
    BID_UINT64 rsq;
    QPI_D_T rtn;
    BID_UINT64 sqlim;
    int cond;
    bid64_from_string(&sqlim, "500001");
    bid64_mul(&rsq, a, a); // rsq = a^2
    bid64_quiet_greater_equal(&cond, &rsq, &sqlim); // If
    if(cond){
        rtn.dtype=QPI_TYPE_INVALID;
        return rtn;
    } else {
        rtn = QPI_frac(&rsq);
        rtn.dtype = QPI_TYPE_SQRT;
        return rtn;
    }
}

// QPI_pi
//
// This function factors a BID64 number into PI*P/Q form to a given accuracy.
// Input:  a:    The BID64 number to be factorized, must be positive
// Output: d:    QPI Object
QPI_D_T QPI_pi(BID_UINT64* a){
    QPI_D_T rtn;
    BID_UINT64 t;
    BID_UINT64 pi;
    bid64_from_string(&pi, "3.14159265358979324");
    bid64_div(&t, a, &pi);
    rtn = QPI_frac(&t);
    rtn.dtype = QPI_TYPE_PI;
    return rtn;
}

// QPI_frac
//
// This function factors a BID64 number into P/Q form to a given accuracy.
// Input:  a:    The BID64 number to be factorized, must be positive
// Output: d:    QPI Object
QPI_D_T QPI_frac(
    BID_UINT64* a
){
    int niter = 1;
    int cond;
    // Prepare
    BID_UINT64 num;
    BID_UINT64 den;
    BID_UINT64 rem;
    BID_UINT64 inum;
    BID_UINT64 iden;
    BID_UINT64 t;
    BID_PQ_t   pq0;
    BID_PQ_t   pq1;
    BID_PQ_t   pq2;
    memcpy(&num, a, sizeof(BID_UINT64));

    // Constants
    BID_UINT64 ten;
    BID_UINT64 acc;
    BID_UINT64 ten_acc;
    double tenf  = 10.0;
    double onef  = 1.0;
    double zerof = 0.0;
    int acci    = QPI_ACC_LIMIT;
    double accf = (double)acci;
    binary64_to_bid64(&ten, &tenf);
    binary64_to_bid64(&den, &onef);
    binary64_to_bid64(&acc, &accf);
    bid64_round_integral_zero(&acc, &acc);
    bid64_pow(&ten_acc, &ten, &acc);

    // First loop to find the number of denominator digits
    bid64_round_integral_zero(&inum, &num);
    while(bid64_quiet_greater(&cond, &num, &inum), cond){
        bid64_mul(&num, &num, &ten);
        bid64_mul(&den, &den, &ten);
        bid64_round_integral_zero(&inum, &num);
    }
    memcpy(&iden, &den, sizeof(BID_UINT64));
    memcpy(&rem, &den, sizeof(BID_UINT64));
    memcpy(&den, &num, sizeof(BID_UINT64));

    // Second loop to find the P/Q factors using continued fraction
    // The inner term should be smaller and smaller until it becomes zero.
    // In this case the fraction process is done and we can eject.
    // If an accuracy / iteration limit is reached before the inner term becomes 
    //   zero, we eject and report it as an failure.

    // Initialize PQ Registers
    binary64_to_bid64(&(pq1.p), &zerof);
    binary64_to_bid64(&(pq2.p), &onef);
    binary64_to_bid64(&(pq1.q), &onef);
    binary64_to_bid64(&(pq2.q), &zerof);
    do {
        BID_UINT64 quo;
        memcpy(&pq0, &pq1, sizeof(BID_PQ_t));
        memcpy(&pq1, &pq2, sizeof(BID_PQ_t));
        memcpy(&num, &den, sizeof(BID_UINT64));
        memcpy(&den, &rem, sizeof(BID_UINT64));
        // c = floor( nu / de )
        bid64_div(&quo, &num, &den);
        bid64_round_integral_zero(&quo, &quo);
        // r = nu mod de
        bid64_fmod(&rem, &num, &den);
        // p2 = c*p1+p0
        bid64_mul(&t, &quo, &(pq1.p));
        bid64_add(&(pq2.p), &(pq0.p), &t);
        // q2 = c*q1+q0
        bid64_mul(&t, &quo, &(pq1.q));
        bid64_add(&(pq2.q), &(pq0.q), &t);

#ifdef QPI_LIM_BY_NITER
        // Calculate Error to see how close we are
        bid64_div(&t, &(pq2.p), &(pq2.q));
        bid64_sub(&t, &t, a);
        bid64_abs(&t, &t);
        bid64_isZero(&cond, &t);
        if(niter++>=acci) break;
#else
        // The is the method to determine end-point in the original QPI alg.
        BID_UINT64 nq, np;
        bid64_isZero(&cond, &rem);
        if(cond) break;
        bid64_mul(&nq, &inum, &(pq2.q));
        bid64_mul(&np, &iden, &(pq2.p));
        bid64_mul(&nq, &nq, &ten_acc);
        bid64_mul(&t,  &np, &ten_acc);
        bid64_sub(&t, &nq, &t);
        bid64_abs(&t, &t);
        bid64_quiet_less(&cond, &t, &np);
        niter++;
#endif /* QPI_LIM_BY_NITER */
     } while(!cond);

#ifdef QPI_DEBUG
    PRINT_BID64("delta",t);
    bid64_div(&t, &(pq2.p), &(pq2.q));
    PRINT_BID64("P/Q",t);
    printf(GRN "Niter = " UGRN "%d\n" COLOR_RESET,niter);
#endif /* QPI_DEBUG */
    QPI_D_T rtn;
    memcpy(&(rtn.pq),&(pq2),sizeof(BID_PQ_t));
    rtn.dtype=QPI_TYPE_FRAC;
    return rtn;
} /* QPI_fraction */

// QPI_make_prime_table: Populate a prime table using a brute-force method
//
int QPI_make_prime_table(int* arr, int ulim){
    int cnt = 0;
    for(int i=0;i<ulim;i++) {
        if (i == 2) goto label_1;
        if (i % 2 == 0) goto label_0;
        for (int j = 3; j * j <= i; j += 2) {
            if (i % j == 0) goto label_0;
        }
label_1:
        arr[i] = 1;
        cnt++;
        goto label_done;
label_0:
        arr[i] = 0;
label_done:;
    }
    return cnt;
}

int prime_table[1000];

int main(int argc, char** argv){
    // QPI demo
    printf("Begin QPI demo\n");

    BID_UINT64 pi;
    bid64_from_string(&pi,"3.141592653589793238462643383279502884197169399375105820974944592307816406286");
    bid64_to_string(buf,&pi);
    printf("PI = %s\n", buf);


    BID_UINT64 a;

    printf("TEST1: 1.23456\n>>> ");
    bid64_from_string(&a,"1.23456");
    QPI_D_T d = QPI_auto(&a);
    QPI_print(d);

    printf("TEST2: SQRT(997)\n>>> ");
    bid64_from_string(&a,"997");
    bid64_sqrt(&a,&a);
    d = QPI_auto(&a);
    QPI_print(d);

    printf("TEST3: SIN(60 deg)\n>>> ");
    BID_UINT64 con60;
    BID_UINT64 con180;
    bid64_from_string(&con60, "60.0");
    bid64_from_string(&con180, "180.0");
    // convert degree to radian
    bid64_div(&a, &con60, &con180);
    bid64_mul(&a,&a,&pi);
    bid64_sin(&a,&a);
    d = QPI_auto(&a);
    QPI_print(d);

    printf("TEST4: PI * 1.23456\n>>> ");
    bid64_from_string(&a,"1.23456");
    bid64_mul(&a,&a,&pi);
    d = QPI_auto(&a);
    QPI_print(d);

    /*
    QPI_make_prime_table(prime_table,sizeof(prime_table)/sizeof(int));
    for(int i=0;i<sizeof(prime_table)/sizeof(int);i++){
        if(prime_table[i]) printf("%d ",i);
    }
    */

    return 0;
} /* main */
