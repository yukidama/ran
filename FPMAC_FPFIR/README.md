## FPMAC & FPFIR

A fully configurable & pipelined, partially IEEE754 compliant floating point multiply-add ALU and a fully pipelined FIR.