`timescale 1ns / 1ps
`default_nettype none

//
// VerilogDC
// Copyright 2023 Anhang Li
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// Booth-4 Encoder
//

module sh4_fpu_br4enc(
	input      [2:0]	bin,
	output reg [2:0]	br4_out
);

localparam  BOOTH_0  = 3'b000;
localparam  BOOTH_P1 = 3'b001;
localparam  BOOTH_P2 = 3'b010;
localparam  BOOTH_N1 = 3'b111;
localparam  BOOTH_N2 = 3'b110;

always @* begin
	br4_out = 3'bxxx;
	case(bin)
		3'b000: br4_out = BOOTH_0;
		3'b001: br4_out = BOOTH_P1;
		3'b010: br4_out = BOOTH_P1;
		3'b011: br4_out = BOOTH_P2;
		3'b100: br4_out = BOOTH_N2;
		3'b101: br4_out = BOOTH_N1;
		3'b110: br4_out = BOOTH_N1;
		3'b111: br4_out = BOOTH_0;
	endcase
end

endmodule /* sh4_fpu_br4enc */