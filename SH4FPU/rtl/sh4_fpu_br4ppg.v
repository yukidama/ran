`timescale 1ns / 1ps
`default_nettype none

//
// VerilogDC
// Copyright 2023 Anhang Li
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// Booth-4 Partial Product Generator
//
module sh4_fpu_br4ppg #(
	parameter DWIDTH = 11
)(
	input [DWIDTH-1:0]			a,
	input [2:0]					br4,
	output reg [DWIDTH:0]		o,
	output                      s
);
	assign s = br4[2];
	always @* begin
		o = {(DWIDTH){1'bx}};
		case(br4)
		3'b000: o = 0;
		3'b010: o = {1'b0,a};
		3'b001:	o = {1'b0,a};
		3'b011: o = {a,1'b0};
		3'b100: o = {~a,1'b1};
		3'b101:	o = {1'b1,~a};
		3'b110:	o = {1'b1,~a};
		3'b111: o = {DWIDTH+1{1'b1}};
		endcase
	end
endmodule /* sh4_fpu_br4ppg */