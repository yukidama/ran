`timescale 1ns / 1ps
`default_nettype none

//
// VerilogDC
// Copyright 2023 Anhang Li
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


module sh4_fpu_lzc#(
    parameter W_IN = 32,            // Must be power of 2, >=2
    parameter W_OUT = $clog2(W_IN), // Don't touch
    parameter RECURSIVE_IMPL = 1    // 1: Use recursive implementation (May not be synthesizable)
                                    // 0: Use one hot (May not be optimizable)
)(
    input wire  [W_IN-1:0] in,
    output wire [W_OUT-1:0] out
);
generate
    if(RECURSIVE_IMPL) begin
        if (W_IN > 2) begin : gen_recurse
            wire [W_OUT-2:0] half_count;
            wire [W_IN / 2-1:0] lhs = in[W_IN / 2 +: W_IN / 2];
            wire [W_IN / 2-1:0] rhs = in[0        +: W_IN / 2];
            wire left_empty = ~|lhs;

            sh4_fpu_lzc #(
                .W_IN (W_IN / 2)
            ) inner (
                .in  (left_empty ? rhs : lhs),
                .out (half_count)
            );
            assign out = {left_empty, half_count};
        end else begin : gen_terminal
            assign out = !in[1];
        end
    end else begin
        // Reverse the number for calculating trailing zeros
        reg [W_IN-1:0] reversed;
        integer i;
        always @(*) begin
            for (i = 0; i < W_IN; i = i + 1)
                reversed[W_IN - 1 - i] = in[i];
        end

        // Extract only the lowest set bit of data reversed
        wire [W_IN-1:0] reversed_lsb = reversed & (-reversed);

        // Given only one bit would be high, it's possible to now get the index in
        // a loop in parallel
        always @(*) begin
            out = 0;
            for (i = 0; i < WIDTH; i = i + 1)
                out = out | (reversed_lsb[i] ? i : 0);
        end
    end
endgenerate
endmodule /* sh4_fpu_lzc */