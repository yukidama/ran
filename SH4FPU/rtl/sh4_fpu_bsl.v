`timescale 1ns / 1ps
`default_nettype none

//
// VerilogDC
// Copyright 2023 Anhang Li
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// Parametrized Barrel Shifter: Left Shift
module sh4_fpu_bsl #(
	parameter SWIDTH = 5
)(
	input [(2**SWIDTH)-1:0]     din,
	input [SWIDTH-1:0]          s,
	input                       filler, // 0: SLL Logic Shift Left
	output [(2**SWIDTH)-1:0]    dout
);

wire [(2**SWIDTH)-1:0]	temp [SWIDTH:0];

assign temp[0] = din;
assign dout    = temp[SWIDTH];

genvar gi;
generate
	for(gi=0;gi<SWIDTH;gi=gi+1) begin : gen_blshift
		assign temp[gi+1] = s[gi] ? {temp[gi],{(2**gi){filler}}} : temp[gi];
	end
endgenerate

endmodule   /* sh4_fpu_bsl */